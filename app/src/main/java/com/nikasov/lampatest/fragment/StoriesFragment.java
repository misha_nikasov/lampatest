package com.nikasov.lampatest.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Callback;
import retrofit2.Response;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.nikasov.lampatest.R;
import com.nikasov.lampatest.adapter.RecyclerAdapter;
import com.nikasov.lampatest.adapter.SliderAdapter;
import com.nikasov.lampatest.server.Item;
import com.nikasov.lampatest.server.ItemList;
import com.nikasov.lampatest.server.Service;
import com.opensooq.pluto.PlutoView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.nikasov.lampatest.activity.MainActivity.TAG;

public class StoriesFragment extends Fragment {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.slider_view)
    PlutoView slider;

    private List<Item> list;

    private RecyclerAdapter adapter;

    public StoriesFragment() {}

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stories, container, false);
        ButterKnife.bind(this, view);

        setHasOptionsMenu(true);

        setUpList(list);
        getList();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpList(list);
    }

    private void getList() {

        Service.getInstance()
                .getServerApi()
                .getAllItems()
                .enqueue(new Callback<ItemList>() {
                    @Override
                    public void onResponse(@NotNull retrofit2.Call<ItemList> call, @NotNull Response<ItemList> response) {
                        ItemList body = response.body();
                        list = body.getItemList();
                        Log.d(TAG, "Response list size: " + list.size());
                        setUpList(list);
                        setSlider(list);
                    }

                    @Override
                    public void onFailure(@NotNull retrofit2.Call<ItemList> call, @NotNull Throwable t) {
                        Log.d(TAG, "Response failed :\n" + t.getLocalizedMessage());
                    }
                });
    }

    private void setUpList(List<Item> items) {

        if (items != null) {
            List<Item> stories = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                stories.add(items.get(i));
            }
            adapter = new RecyclerAdapter(getContext(), stories);
        }

        else {
            items = new ArrayList<>();
            adapter = new RecyclerAdapter(getContext(), items);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

        try {
            recyclerView.invalidateItemDecorations();
            DividerItemDecoration decoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
            decoration.setDrawable(getContext().getDrawable(R.drawable.divider));
            recyclerView.addItemDecoration(decoration);
        }
        catch (Exception e){
            Log.d(TAG, e.getLocalizedMessage());
        }

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(true);
    }

    private void setSlider(List<Item> items){

        List<Item> slideList = new ArrayList<>();

        int random = new Random().nextInt(items.size()) - 5;

        while (random < 0) {
            random = new Random().nextInt(items.size()) - 5;
        }

        for (int i = random; i < items.size(); i++) {
            if (items.get(i).getImage().getUrl() != null){
                slideList.add(items.get(i));
            }
            if (slideList.size() == 5) {
                break;
            }
        }

        SliderAdapter adapter = new SliderAdapter(slideList, getContext());
        slider.create(adapter,getLifecycle());
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main_manu, menu);

        Log.d(TAG, "menu inflated");

        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) search.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }
}
