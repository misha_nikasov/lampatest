package com.nikasov.lampatest.server;

import retrofit2.Call;
import retrofit2.http.GET;

public interface LampaApi {

    @GET("api/v1.0/products/")
    Call<ItemList> getAllItems();
}
