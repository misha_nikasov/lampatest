package com.nikasov.lampatest.server;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemList {

    @SerializedName("results")
    private List<Item> itemList;

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

}
