package com.nikasov.lampatest.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.nikasov.lampatest.R;
import com.nikasov.lampatest.server.Item;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.nikasov.lampatest.activity.MainActivity.TAG;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> implements Filterable {

    private Context context;
    private List<Item> itemList;

    private List<Item> itemListSource;

    public RecyclerAdapter(Context context, List<Item> itemList) {
        this.context = context;
        this.itemList = itemList;

        itemListSource = new ArrayList<>(itemList);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Item item = itemList.get(position);

        String source = context.getResources().getString(R.string.source) + " " + String.valueOf(item.getId());
        String date = context.getResources().getString(R.string.date) + " " + String.valueOf(item.getPrice()) + " ";

        holder.title.setText(item.getName());
        holder.source.setText(source);
        holder.date.setText(date);

        Picasso.get()
                .load(item.getImage().getUrl())
                .fit()
                .centerCrop()
                .into(holder.image);

        if (item.getImage().getUrl() == null)
            holder.image.setVisibility(View.GONE);
        else
            holder.image.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.source)
        TextView source;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.image)
        ImageView image;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Item> filterItemList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0){
                filterItemList.addAll(itemListSource);
            }
            else {
                String pattern = constraint.toString().toLowerCase().trim();

                for (Item item : itemListSource){
                    if (item.getName().toLowerCase().contains(pattern)){
                        filterItemList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filterItemList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            itemList.clear();
            itemList.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };
}
