package com.nikasov.lampatest.adapter;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nikasov.lampatest.R;
import com.nikasov.lampatest.server.Item;
import com.opensooq.pluto.base.PlutoAdapter;
import com.opensooq.pluto.base.PlutoViewHolder;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.nikasov.lampatest.activity.MainActivity.TAG;

public class SliderAdapter extends PlutoAdapter<Item, SliderAdapter.MyViewHolder> {

    private Context context;

    public SliderAdapter(List<Item> items, Context context) {
        super(items);
        this.context = context;
    }

    @NotNull
    @Override
    public MyViewHolder getViewHolder(@NotNull ViewGroup viewGroup, int i) {
        return new MyViewHolder(viewGroup, R.layout.item_slide_layout);
    }

    class MyViewHolder extends PlutoViewHolder<Item> {

        TextView title;
        TextView source;
        TextView date;
        ImageView image;

        MyViewHolder(ViewGroup parent, int itemLayoutId) {
            super(parent, itemLayoutId);

            title = getView(R.id.title);
            source = getView(R.id.source);
            date = getView(R.id.date);
            image = getView(R.id.image);
        }

        @Override
        public void set(Item item, int pos) {
            String sourceStr = context.getResources().getString(R.string.source) + " " + String.valueOf(item.getId());
            String dateStr = context.getResources().getString(R.string.date) + " " + String.valueOf(item.getPrice()) + " ";

            title.setText(item.getName());
            source.setText(sourceStr);
            date.setText(dateStr);

            Picasso.get()
                    .load(item.getImage().getUrl())
                    .fit()
                    .centerCrop()
                    .into(image);
        }
    }
}
